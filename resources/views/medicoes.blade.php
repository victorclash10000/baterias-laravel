@extends('templates.base')

    @section('conteudo')
    <main>
        <h1>Medições</h1>
        <hr>
        <h2>Resultados:</h2>
        <table class="table table-dark table-striped table-bordered" id="tbDados">
            <tr>
                <th>Pilha/Bateria</th>
                <th>Tensão Nominal (V)</th>
                <th>Capacidade de Corrente</th>
                <th>Tensão sem carga (V)</th>
                <th>Tensão com carga (V)</th>
                <th>Resistência de carga(ohm)</th>
                <th>Resistência interna (ohm)</th>
            </tr>
            <tr>
                <th>Duracell AA</th>
                <th>1.5</th>
                <th>2800mAh</th>
                <th>1.304</th>
                <th>1.287</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Duracell plus AAA</th>
                <th>1.5</th>
                <th>1200</th>
                <th>0.986</th>
                <th>0.820</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Luatek</th>
                <th>3.7</th>
                <th>1200mAh</th>
                <th>2.413</th>
                <th>2.414</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Elgin</th>
                <th>9.0</th>
                <th>250mAh</th>
                <th>7.17</th>
                <th>2.0837</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Golite</th>
                <th>9.0</th>
                <th>500mAh</th>
                <th>5.34</th>
                <th>29.2m</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Panasonic AA</th>
                <th>1.5</th>
                <th>2500</th>
                @extends('templates.base')

@section('conteudo')  <th>1.378</th>
                <th>1.340</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Phillips AAA</th>
                <th>5.0</th>
                <th>1200</th>
                <th>1.368</th>
                <th>1.320</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>JVX</th>
                <th>3.7</th>
                <th>9800mAh</th>
                <th>2.401</th>
                <th>2.407</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Unipower</th>
                <th>12.0</th>
                <th>7.0Ah</th>
                <th>10.48</th>
                <th>10.30</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
            <tr>
                <th>Freedom</th>
            <tr>
                <th>Phillips AAA</th>
                <th>5.0</th>
                <th>1200</th>
                <th>1.368</th>
                <th>1.320</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
                <th>12.0</th>
                <th>26000</th>
                <th>10.68</th>
                <th>10.69</th>
                <th>23.8</th>
                <th>x</th>
            </tr>
    </main>
    @endsection
    
    @section('rodape')
        <h4>rodapé da pagina principal<h4>
    @endsection
